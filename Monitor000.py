import cv2
import numpy as np
import PIL.ImageGrab
import win32gui
import pyautogui
import time
from GameKeyboardAndMouse import *


# The first ever created Monitor.
class Monitor000:
    name = 'Tragic Solitude'

    gameName = 'Google Chrome - T-rex runner'
    gameWindowName = 'T-rex runner'
    windowHandler = None
    screen = None

    templates = {}
    matchMethod = cv2.TM_CCOEFF_NORMED

    def __init__(self):
        # Obtaining a handler for the game window
        self.windowHandler = win32gui.FindWindow(None, self.gameWindowName)
        # Reading the templates
        self.readTemplates()

    # Getting all the templates into memory
    def readTemplates(self):
        # TODO: Actually define and read all of them -> maybe as dict {file_name -> img}
        self.templates['replay'] = cv2.imread('environment/replay.png', 0)

    def gameStart(self):
        # Focus on the game window
        win32gui.SetForegroundWindow(self.windowHandler)
        # Press a LeftMouseClick on it for actual focus
        pyautogui.click(x=100, y=100)
        # Run!
        InputKey(SPACE)

    def checkGameOver(self):
        # Template matching for replay button
        res = cv2.matchTemplate(self.screen, self.templates['replay'], self.matchMethod)
        # Check if we've got a match with at least 80% accuracy for max_val
        _, max_val, _, _ = cv2.minMaxLoc(res)

        return max_val > 0.80


    # Taking a Screenshot for the game window and saving it internally
    def takeScreenshot(self):
        self.screen = np.array(PIL.ImageGrab.grab().convert('RGB'))
        self.screen = self.screen[:, :, 0]


    def simulation(self):
        # Start the game
        self.gameStart()
#       TODO: Implement a loop that awaits for user key input to terminate (instead of sleep(10))
        time.sleep(10)
        if self.checkGameOver():
            print('Reluam jocul')
            InputKey(SPACE)
